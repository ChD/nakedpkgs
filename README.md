# nakeDeb packages sources

les sources nakedpkgs contiennent de quoi construire les paquets debian intégrés à [nakeDeb](https://nakedeb.arpinux.org)

ces paquets sont disponibles depuis le [dépôt nakeDeb](https://nakedeb.arpinux.org/repo)

## contents

* [bashmount](bashmount) : utilitaire de gestion de volumes internes ou amovibles
* [clarity-icon-theme](clarity-icon-theme) : thème d'icône par défaut pour nakeDeb
* [eyecandy](eyecandy) : utilitaire de gestion pour picom (ombres et transparences)
* [fluxbox-automenu](fluxbox-automenu) : un menu auto-généré pour fluxbox par @prx
* [info4help](info4help) : outil d'information système avec envoi de rapport préformaté pour les forums par @3hg
* [nakedbase](nakedbase) : version, GRUB config et manuels pour nakeDeb
* [nakedconky](nakedconky) : différentes configurations pour conky et un menu pour en changer
* [nakeddots](nakeddots) : fichiers cachés de l'utilisateur sur nakedeb
* [nakedfluxbox](nakedfluxbox) : configuration de fluxbox sur nakeDeb
* [nakedhelp](nakedhelp) : centre d'aide pour nakeDeb (sert aussi pour le site principal)
* [nakedi3wm](nakedi3wm) : configuration de i3wm sur nakeDeb
* [nakedlook](nakedlook) : nakeDeb artwork
* [nakedroot](nakedroot) : fichiers cachés de root sur nakeDeb
* [nakedtools](nakedtools) : petits outils pour nakeDeb
* [tkmenu](tkmenu) : menu graphique rapide et léger par @prx
