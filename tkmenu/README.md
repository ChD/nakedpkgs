# Tkmenu

menu rapide et léger par @prx

fork du [tkmenu original 3hg](https://framagit.org/3hg/tkmenu)

Licence MIT

--------

mods & adds:

* ajout de catégories
* passage au thème Nord
* modification des lanceurs pour nakeDeb
* cacher le bouton closeafterlaunch
* ajout du bouton "cfg"
* suppression des icônes par défaut
* ajout des fichiers debian
* ajout du thème d'icônes depuis AnyColorYouLike

--------

howto:

```
# apt install equivs lintian
$ cd tkmenu/
$ ./mkdeb
$ lintian ./tkmenu_$version.deb
# dpkg -i tkmenu_$version.deb
$ tkmenu
```
